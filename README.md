
## Syntaxe en ligne de commande

```java ColesPlanner [options] problem1.pddl [problem2.pddl ... problemN.pddl]```

options:

 * `-runs N` Nombre d'essais par problème (défaut: 1).
 * `-prefix "path/to/problems/"` Pour éviter d'avoir à écrire plusieurs fois le même début de chemin.
 * `-outSamples` Écrit les résultats dans un fichier CSV.
 * `-outPlans` Écrit les plans trouvés dans un fichier CSV.
 * `-out` Équivalent à `-outSamples -outPlans`

---

## ...