/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import distributions.Distribution;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Charline David 
 */
public class DistributionTests {
    public DistributionTests() {}
    
    @BeforeClass
    public static void setUpClass() throws Exception {}

    @AfterClass
    public static void tearDownClass() throws Exception {}

    @Before
    public void setUp() {}

    @After
    public void tearDown() {}

    @Test
    public void tobeRecoded() {}
    
    //@Test
    public void addConstantConstant() throws Exception {
        Distribution c1 = new Distribution(1);
        Distribution c2 = new Distribution(2);
        assertEquals(c1.add(c2), new Distribution(3));
    }
    
    //@Test
    public void addConstantNormal() throws Exception {
        Distribution c = new Distribution(1);
        Distribution n = new Distribution("N",0,3);
        assertEquals(c.add(n), new Distribution("N",1,3));
    }
    
    //@Test
    public void addNormalConstant() throws Exception {
        Distribution c = new Distribution(1);
        Distribution n = new Distribution("N",0,3);
        assertEquals(n.add(c), new Distribution("N",1,3));
    }
}
