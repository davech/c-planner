package graphs;
import java.util.*;

/**
 *
 * @author Charline David 
 */
public class TreeNode<T> {
    protected T data;
    protected TreeNode<T> parent;
    protected List<TreeNode<T>> children;
    
    protected TreeNode() {
        children = new ArrayList<>();
    }
    
    public TreeNode(T rootData) {
        data = rootData;
        children = new ArrayList<>();
    }
    
    public TreeNode(List<T> nodes) {
        children = new ArrayList<>();
        if (nodes.size() > 0) {
            data = nodes.get(0);
            
            if (nodes.size() > 1) {
                TreeNode<T> t = this;
                for (T node : nodes) {
                    TreeNode<T> n = new TreeNode(node);
                    t.addChild(n);
                    t = n;
                }
            }
        }
    }
    
    public boolean isEmpty() {
        return (data == null && children.isEmpty());
    }
    public int size() {
        int s = (isEmpty() ? 0 : 1);
        for (TreeNode c : children) {
            s += c.size();
        }
        return s;
    }
    
    public T data() {
        return data;
    }
    public List<TreeNode<T>> children() {
        return children;
    }
    public TreeNode<T> parent() {
        return parent;
    }
    
    public void addChild(TreeNode<T> n) {
        if (!children.contains(n)) {
            children.add(n);
            if (n.parent != null) {
                n.parent.children.remove(n);
            }
            n.parent = this;
        }
    }
    public void addChild(T c) {
        TreeNode<T> child = new TreeNode(c);
        addChild(child);
    }
    
    public int childrenCount() {
        return children.size();
    }
    public int leavesCount() {
        if (children.size() == 0) {
            return 1;
        }
        int leaves = 0;
        for (TreeNode c : children) {
            leaves += c.leavesCount();
        }
        return leaves;
    }
    public List<TreeNode<T>> getLeaves() {
        List<TreeNode<T>> leaves = new ArrayList<>();
        if (children.isEmpty()) {
            leaves.add(this);
        } else {
            for (TreeNode c : children) {
                leaves.addAll(c.getLeaves());
            }
        }
        return leaves;
    }
    public TreeNode<T> getChild(int i) {
        return children.get(i);
    }
    
    @Override public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o instanceof TreeNode) {
            if (data == null) {
                return (((TreeNode<T>)o).data == null);
            }
            return data.equals(((TreeNode<T>)o).data);
        } else {
            return false;
        }
    }
    
    @Override public String toString() {
        if (isEmpty()) {
            return "";
        }
        String s = "" + data;
        for (TreeNode c : children) {
            String prefix = (c == children.get(children.size()-1) ? "\n└── " : "\n├── ");
            String infix  = (c == children.get(children.size()-1) ? "\n    " : "\n│   ");
            s += prefix + c.toString().replaceAll("\n", infix);
        }
        return s;
    }
    
    public String toStringAlignLeft() {
        if (isEmpty()) {
            return "";
        }
        String s = "" + data;
        if (children.size() == 1) {
            s += "\n" + children.get(0).toStringAlignLeft();
        } else {
            for (TreeNode c : children) {
                String prefix = (c == children.get(children.size()-1) ? "\n└── " : "\n├── ");
                String infix  = (c == children.get(children.size()-1) ? "\n    " : "\n│   ");
                s += prefix + c.toStringAlignLeft().replaceAll("\n", infix);
            }
        }
        return s;
    }
    
    public TreeNode<T> reverseBranch() {
        TreeNode<T> root = new TreeNode<>(data);
        TreeNode<T> r = root;
        TreeNode<T> n = this;
        while(n.parent != null) {
            TreeNode<T> c = new TreeNode<>(n.parent.data);
            r.addChild(c);
            r = c;
            n = n.parent;
        }
        return root;
    }
    public TreeNode<T> reverse() {
        TreeNode<T> root = new TreeNode<>();
        for (TreeNode<T> leaf : root().getLeaves()) {
            if (root.isEmpty()) {
                root = leaf.reverseBranch();
            } else {
                root = root.merge(leaf.reverseBranch());
            }
        }
        return root;
    }
    public TreeNode<T> root() {
        TreeNode<T> n = this;
        while(n.parent != null) {
            n = n.parent;
        }
        return n;
    }
    public TreeNode<T> getBranch() {
        return reverseBranch().getLeaves().get(0).reverseBranch();
    }
    public List<TreeNode<T>> getChildBranches() {
        List<TreeNode<T>> branches = new ArrayList<>();
        TreeNode<T> n = clone();
        List<TreeNode<T>> leaves = n.getLeaves();
        for (TreeNode<T> leaf : leaves) {
            branches.add(leaf.getBranch());
        }
        return branches;
    }
    
    public TreeNode<T> merge(TreeNode<T> o) {
        TreeNode<T> root;
        if (o == null || o.isEmpty()) {
            return clone();
            
        } else if (isEmpty()) {
            return o.clone();
            
        } else if (equals(o)) {
            root = new TreeNode<>(data);
            for(TreeNode<T> c1 : children) {
                if (o.children.contains(c1)) {
                    TreeNode<T> c2 = o.children.get(o.children.indexOf(c1));
                    root.addChild(c1.merge(c2));
                } else {
                    root.addChild(c1.clone());
                }
            }
            for(TreeNode<T> c2 : o.children) {
                if (!children.contains(c2)) {
                    root.addChild(c2.clone());
                }
            }
            
        } else {
            root = new TreeNode<>();
            root.addChild(clone());
            root.addChild(o.clone());
        }
        return root;
    }
    
    public void mergeIn(TreeNode<T> o) {
        if (o == null || o.isEmpty()) {
            return;
            
        } else if (isEmpty() || equals(o)) {
            if (isEmpty()) {
                data = o.data;
            }
            
            for(TreeNode<T> c2 : o.children) {
                if (children.contains(c2)) {
                    TreeNode<T> c1 = children.get(children.indexOf(c2));
                    c1.mergeIn(c2);
                } else {
                    addChild(c2.clone());
                }
            }
            
        } else {
            TreeNode<T> root = (parent == null ? new TreeNode<>() : parent);
            root.addChild(this);
            root.addChild(o.clone());
        }
    }
    
    @Override public TreeNode<T> clone() {
        TreeNode<T> root = new TreeNode(data);
        for (TreeNode<T> c : children) {
            root.addChild(c.clone());
        }
        return root;
    }
    
    public int height() {
        int h = 0;
        for (TreeNode<T> c : children) {
            h = Math.max(c.height(), h);
        }
        return h + 1;
    }
    public int leftmostHeight() {
        int h = 0;
        TreeNode<T> n = this;
        while (n.hasChildren()) {
           h += 1;
           n = n.getChild(0);
        }
        return h + 1;
    }
    public List<TreeNode<T>> getLevel(int i) {
        List<TreeNode<T>> nodes = new ArrayList<>();
        if (i == 0) {
            nodes.add(this);
            return nodes;
        } else {
            i -= 1;
            for (TreeNode<T> c : children) {
                nodes.addAll(c.getLevel(i));
            }
            return nodes;
        }
    }
    public void setData(T data) {
        this.data = data;
    }

    public boolean hasChildren() {
        return !children.isEmpty();
    }

    public void cutBranch() {
        if (parent != null) {
            parent.children.remove(this);
            if (!parent.hasChildren()) {
                parent.cutBranch();
            }
            parent = null;
        }
    }

    public TreeNode<T> travel(TreeNode<T> branch) {
        if (children.contains(branch)) {
            TreeNode<T> child = children.get(children.indexOf(branch));
            if (branch.hasChildren()) {
                return child.travel(branch.getChild(0));
            } else {
                return child;
            }
        } else {
            return this;
        }
    }
}
