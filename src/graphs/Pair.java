
package graphs;

/**
 *
 * @author Charline David 
 */
public class Pair<F,S> {
    private F first;
    private S second;
    public Pair(F f, S s) {
        first = f;
        second = s;
    }
    public F first() {
        return first;
    }
    public S second() {
        return second;
    }
    @Override public String toString() { //〈〉
        return "〈" + first + "," + second + "〉";
    }
    
    @Override public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (o instanceof Pair) {
            Pair<F,S> p = (Pair)o;
            if (first == null) {
                if (second == null) {
                    return (p.first == null && p.second == null);
                } else {
                    return (p.first == null && second.equals(p.second));
                }
            } else if (second == null) {
                return (first.equals(p.first) && p.second == null);
            } else {
                return (first.equals(p.first) && second.equals(p.second));
            }
        }
        return false;
    }
}
