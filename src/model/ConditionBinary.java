
package model;

import java.util.*;

/**
 *
 * @author Charline David 
 */
public abstract class ConditionBinary extends Condition {
    protected Condition p1, p2;
    
    public ConditionBinary(Condition a, Condition b, Condition... args) {
        p1 = a;
        p2 = b;
        for (Condition c : args) {
            p2 = newConditionOfSameType(c, p2);
        }
    }
    
    public Condition clean() {
        List<Condition> oldLeaves = getLeaves();
        List<Condition> newLeaves = new ArrayList<>();
        for (Condition c : oldLeaves) {
            if (!newLeaves.contains(c)) {
                newLeaves.add(c);
            }
        }
        if (newLeaves.isEmpty()) {
            return new ConditionTrue();
        } else if (newLeaves.size() == 1) {
            return newLeaves.get(0);
        } else {
            Condition res = newConditionOfSameType(newLeaves.get(0), newLeaves.get(1));
            for (Condition c : newLeaves.subList(2, newLeaves.size())) {
                res = newConditionOfSameType(c, res);
            }
            return res;
        }
    }
    
    protected abstract ConditionBinary newConditionOfSameType(Condition c1, Condition c2);
    
    @Override public boolean check(State state) {
        return check(p1.check(state), p2.check(state));
    }

    @Override public boolean check(State state, double theta) {
        return check(p1.check(state, theta), p2.check(state, theta));
    }

    @Override public List<String> getTargets() {
        List<String> list = new ArrayList<>();
        list.addAll(p1.getTargets());
        list.addAll(p2.getTargets());
        return list;
    }
    
    @Override public List<Condition> getLeaves() {
        List<Condition> list = new ArrayList<>();
        list.addAll(p1.getLeaves());
        list.addAll(p2.getLeaves());
        return list;
    }

    @Override public boolean involvesConjuction() {
        return (p1.involvesConjuction() || p2.involvesConjuction());
    }
    
    @Override public String toString() {
        List<Condition> leaves = getLeaves();
        String s = "(" + stringOperator();
        for (Condition leaf : leaves) {
            s += " " + leaf;
        }
        s += ")";
        return s;
    }
    
    @Override public boolean reachedBy(Action a) {
        return check(p1.reachedBy(a), p2.reachedBy(a));
    }
    @Override public boolean reachedPartiallyBy(Action a) {
        return (p1.reachedBy(a) || p2.reachedBy(a));
    }
    @Override public boolean violatedBy(Action a) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    @Override public boolean violatedPartiallyBy(Action a) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override public Condition reverse() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    protected abstract boolean check(boolean c1, boolean c2);
    protected abstract String stringOperator();
    
}
