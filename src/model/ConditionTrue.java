
package model;

import java.util.*;

/**
 *
 * @author Charline David 
 */
public class ConditionTrue extends Condition {

    @Override public boolean check(State state) {
        return true;
    }
    @Override public boolean check(State state, double theta) {
        return check(state);
    }

    @Override public List<String> getTargets() {
        return new ArrayList<String>();
    }

    @Override public List<Condition> getLeaves() {
        List<Condition> list = new ArrayList<>();
        list.add(this);
        return list;
    }
    
    @Override public String toString() {
        return "(True)";
    }
    
    @Override public boolean involvesConjuction() {
        return false;
    }
    
    @Override public boolean reachedBy(Action a) {
        return false;
    }
    @Override public boolean reachedPartiallyBy(Action a) {
        return false;
    }
    @Override public boolean violatedBy(Action a) {
        return false;
    }
    @Override public boolean violatedPartiallyBy(Action a) {
        return false;
    }
    
    @Override public Condition clean() {
        return this;
    }
    
    @Override public Condition reverse() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
