
package model;

import java.util.*;

/**
 *
 * @author Charline David 
 */
public abstract class Condition {
    public abstract boolean check(State state);
    public abstract boolean check(State state, double theta);
    public abstract boolean reachedBy(Action a);
    public abstract boolean reachedPartiallyBy(Action a);
    public abstract boolean violatedBy(Action a);
    public abstract boolean violatedPartiallyBy(Action a);
    public abstract List<String> getTargets();
    public abstract List<Condition> getLeaves();
    public abstract Condition clean();
    public abstract Condition reverse();
    
    public abstract boolean involvesConjuction();
    
    @Override public boolean equals(Object o) {
        return toString().equals(o.toString());
    }
}
