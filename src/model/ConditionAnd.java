package model;

/**
 *
 * @author Charline David 
 */
public class ConditionAnd extends ConditionBinary {
    public ConditionAnd(Condition a, Condition b, Condition... args) {
        super(a, b, args);
    }
    
    @Override protected boolean check(boolean c1, boolean c2) {
        return (c1 && c2);
    }
    
    @Override protected String stringOperator() {
        return "and";
    }
    
    @Override public boolean involvesConjuction() {
        return true;
    }

    @Override protected ConditionBinary newConditionOfSameType(Condition c1, Condition c2) {
        return new ConditionAnd(c1,c2);
    }
}
