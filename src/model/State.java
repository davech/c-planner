
package model;

import colesplanner.*;
import java.util.*;

/**
 *
 * @author Charline David 
 */
public abstract class State {
    public static class InitialStateBuilder {
        private State initialState, worldState;
        private ArrayList<Action> cleanActions;
        
        public InitialStateBuilder(State fromState, ArrayList<Action> actions) {
            Set<String> modifiedEffects = getModifiedEffects(actions);
            worldState = asWorldState(modifiedEffects, fromState);
            initialState = asInitialState(modifiedEffects, fromState);
            cleanActions = cleanActions(modifiedEffects, actions);
            worldState.data.facts = new ArrayList<>();
        }
        
        public State getWorldState() {
            return worldState;
        }
        public State getInitialState() {
            return initialState;
        }
        public ArrayList<Action> getCleanActions() {
            return cleanActions;
        }
        
        private Set<String> getModifiedEffects(ArrayList<Action> actions) {
            Set<String> modifiedEffects = new HashSet<>();
            for (Action a : actions) {
                ArrayList<String> eff = a.effects();
                for (String e : eff) {
                    modifiedEffects.add((e.indexOf(' ') >= 0 ? e.substring(0, e.indexOf(' ')+1) : e));
                }
            }
            return modifiedEffects;
        }
        private State asWorldState(Set<String> modifiedEffects, State fromState) {
            ArrayList<String> f = getFacts(modifiedEffects, fromState, false);
            HashMap<String,NumericVariable> v = getVariables(modifiedEffects, fromState, false);
            return new StateImpl(f, v);
        }
        private State asInitialState(Set<String> modifiedEffects, State fromState) {
            ArrayList<String> f = getFacts(modifiedEffects, fromState, true);
            HashMap<String,NumericVariable> v = getVariables(modifiedEffects, fromState, true);
            return new StateImpl(f, v); //, worldState
        }
        /**
        * @return facts that are modified by NO action (if wantDynamic is false) or 
        *  facts that are modified by at least one action (if wantDynamic is true).
        */
        private ArrayList<String> getFacts(Set<String> modifiedEffects, State fromState, boolean wantDynamic) {
            ArrayList<String> wf = new ArrayList<>();
            for (String f : fromState.data.facts) {
                boolean isDynamicFact = false;
                for (String eff : modifiedEffects) {
                    if (f.startsWith(eff)) {
                        isDynamicFact = true;
                        break;
                    }
                }
                if ((isDynamicFact && wantDynamic) || (!isDynamicFact && !wantDynamic)) {
                    wf.add(f);
                }
            }
            return wf;
        }
        /**
         * @return variables that are modified by NO action (if wantDynamic is false) or 
         *  variables that are modified by at least one action (if wantDynamic is true).
         */
        private HashMap<String,NumericVariable> getVariables(Set<String> modifiedEffects, State fromState, boolean wantDynamic) {
            HashMap<String,NumericVariable> wv = new HashMap<>();
            for (String v : fromState.data.variables.keySet()) {
                boolean isDynamicVariable = false;
                for (String eff : modifiedEffects) {
                    if (v.startsWith(eff)) {
                        isDynamicVariable = true;
                        break;
                    }
                }
                if ((isDynamicVariable && wantDynamic) || (!isDynamicVariable && !wantDynamic)) {
                    wv.put(v, fromState.data.variables.get(v));
                }
            }
            return wv;
        }
        
        private ArrayList<Action> cleanActions(Set<String> modifiedEffects, ArrayList<Action> actions) {
            ArrayList<Action> newActions = new ArrayList<>();
            for (Action a : actions) {
                try {
                    Action modifiedAction = a.asDynamic(worldState, modifiedEffects);
                    newActions.add(modifiedAction);
                } catch (UnsupportedOperationException e) {}
            }
            return newActions;
        }
    }
    
    public static class Data implements Cloneable {
        public ArrayList<String> facts;
        public HashMap<String,NumericVariable> variables;
        //public State worldState;
        public Data() {
            this.facts = new ArrayList<>();
            this.variables = new HashMap<>();
        }
        public Data(ArrayList<String> f, HashMap<String,NumericVariable> v) {
            this.facts = new ArrayList<>();
            this.variables = new HashMap<>();
            for (String key : f) {
                this.facts.add(key);
            }
            for (String k : v.keySet()) {
                this.variables.put(k, v.get(k).clone());
            }
        }
        @Override public Data clone() {
            Data d = new Data(facts, variables);
            /*if (worldState != null) {
                d.worldState = worldState.clone();
            }*/
            return d;
        }
    }
    
    protected Data data;
    
    protected abstract String className();
    public abstract State apply(Action a);
    
    /*public State clear() {
        ArrayList<String> f = new ArrayList<>();
        HashMap<String,NumericVariable> v = new HashMap<>();
        if (this instanceof StateImpl) {
            return new StateImpl(f, v); //, data.worldState
        } else if (this instanceof FactLayer) {
            return new FactLayer(f, v); //, data.worldState
        }
        throw new UnsupportedOperationException("State not cleared." );
    }*/
    
    public State() {
        data = new Data();
    }
    public State(ArrayList<String> f, HashMap<String,NumericVariable> v) {
        data = new Data(f, v);
    }
    /*public State(ArrayList<String> f, HashMap<String,NumericVariable> v, State w) {
        data = new Data(f, v);
        data.worldState = w;
    }*/
    public State(State s) {
        data = s.data.clone();
    }
    public State (Data d) {
        data = d.clone();
    }
    
    /*public Action toAction(String name) {
        Condition pre = new ConditionAssignment(name);
        
        ArrayList<UpdateFact> eff = new ArrayList<>();
        for (String f : data.facts) {
            eff.add(new UpdateFact(f, true));
        }
        
        ArrayList<UpdateVariable> effNum = new ArrayList<>();
        for (String v : data.variables.keySet()) {
            effNum.add(new UpdateVariable(v, UpdateVariable.OP.ASSIGN, data.variables.get(v).getValue()));
        }
        
        return new Action(name, pre, eff, effNum);
    }*/
    
    public State toMeanValue() {
        HashMap<String,NumericVariable> vars = new HashMap<>();
        for (NumericVariable v : data.variables.values()) {
            vars.put(v.getKey(), v.toMeanValue());
        }
        State s = clone();
        s.data.variables = vars;
        return s;
    }
    public State simulate() {
        HashMap<String,NumericVariable> vars = new HashMap<>();
        for (NumericVariable v : data.variables.values()) {
            vars.put(v.getKey(), v.sample());
        }
        State s = clone();
        s.data.variables = vars;
        return s;
    }
    
    public boolean isApplicable(Action a) {
        return a.getPre().check(this);
    }
    
    public Set<String> getVariableKeys() {
        return data.variables.keySet();
    }
    public NumericVariable getVariable(String key) {
        if (PlanningProblem.worldState != null) {
            NumericVariable v = PlanningProblem.worldState.data.variables.get(key);
            if (v != null) {
                return v;
            }
        }
        return data.variables.get(key);
    }
    public State setVariable(NumericVariable v) {
        State s = clone();
        s.data.variables.put(v.getKey(), v);
        return s;
    }
    
    public List<String> getFactKeys() {
        return (ArrayList) data.facts.clone();
    }
    public boolean checkFact(String key) {
        if (!data.facts.contains(key)) {
            return false;
        }
        return true;
    }
    public State setFact(String key, boolean value) {
        State s = clone();
        if (value) {
            if (!s.data.facts.contains(key)) {
                s.data.facts.add(key);
            }
        } else {
            s.data.facts.remove(key);
        }
        return s;
    }
    
    @Override public State clone() {
        if (this instanceof StateImpl) {
            return new StateImpl(data);
        } else if (this instanceof FactLayer) {
            return new FactLayer(data);
        }
        throw new UnsupportedOperationException("State not cloned." );
    }
    
    @Override public String toString() {
        String s = className() + " = {";
        if (data.variables.size() > 0) {
            NumericVariable[] values = data.variables.values().toArray(new NumericVariable[data.variables.size()]);
            Arrays.sort(values, new Comparator<NumericVariable>() {
                public int compare(NumericVariable v1, NumericVariable v2) {
                    return v1.toString().compareTo(v2.toString());
                }
            });
            s += "\n\tv = { " + values[0].toString() + ";";
            for (NumericVariable v : Arrays.copyOfRange(values, 1, values.length)) {
                s += "\n\t      " + v.toString() + ";";
            }
            s+= " }";
        }
        if (data.facts.size() > 0) {
            Collections.sort(data.facts);
            s += "\n\tF = { " + data.facts.get(0) + ";";
            for (String f : data.facts.subList(1, data.facts.size())) {
                s += "\n\t      " + f + ";";
            }
            s+= " }";
        }
        return s + "\n}";
    }
}
