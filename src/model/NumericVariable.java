
package model;

import distributions.Distribution;

/**
 *
 * @author Charline David 
 */
public class NumericVariable {
    private String key;
    private Distribution value;
    
    public NumericVariable(String k, double v) {
        key = k;
        value = new Distribution(v);
    }
    public NumericVariable(String k, Distribution d) {
        key = k;
        value = d;
    }
    
    public String getKey() {
        return key;
    }
    public Distribution getValue() {
        return value;
    }
    public NumericVariable toMeanValue() {
        return new NumericVariable(key, value.toMeanValue());
    }
    public NumericVariable sample() {
        return new NumericVariable(key, value.sample());
    }
    
    @Override public NumericVariable clone() {
        return new NumericVariable(key, value.clone());
    }
    @Override public String toString() {
        return key + "=" + value.toString();
    }
}
