
package model;

import java.util.*;

/**
 *
 * @author Charline David 
 */
public class ConditionAssignment extends Condition {
    protected String fact;
    
    public ConditionAssignment(String f) {
        fact = f;
    }
    
    @Override public boolean check(State state) {
        return state.checkFact(fact);
    }
    @Override public boolean check(State state, double theta) {
        return check(state);
    }
    
    @Override public String toString() {
        return fact;
    }
    
    @Override public List<String> getTargets() {
        List<String> list = new ArrayList<>();
        list.add(fact);
        return list;
    }
    
    @Override public List<Condition> getLeaves() {
        List<Condition> list = new ArrayList<>();
        list.add(this);
        return list;
    }
    
    @Override public boolean involvesConjuction() {
        return false;
    }
    
    @Override public boolean reachedBy(Action a) {
        Boolean b = a.getFactUpdateValue(fact);
        if (b == null) {
            return false;
        }
        return b;
    }
    @Override public boolean reachedPartiallyBy(Action a) {
        return reachedBy(a);
    }
    @Override public boolean violatedBy(Action a) {
        Boolean b = a.getFactUpdateValue(fact);
        if (b == null) {
            return false;
        }
        return !b;
    }
    @Override public boolean violatedPartiallyBy(Action a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override public Condition clean() {
        return this;
    }

    @Override public Condition reverse() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
