package model;

import java.util.List;

/**
 *
 * @author Charline David 
 */
public class Goal extends Condition {
    private Condition condition;
    private double failureCost;
    private String name;
    
    public Goal(Condition cond, double cst) {
        name = "";
        condition = cond;
        failureCost = cst;
    }
    public Goal(String n, Condition cond, double cst) {
        name = n;
        condition = cond;
        failureCost = cst;
    }
    
    public double getCost() {
        return failureCost;
    }
    public double checkCost(State state) {
        if (!condition.check(state)) {
            return failureCost;
        } else {
            return 0;
        }
    }
    public Condition toCondition() {
        return condition;
    }
    
    public String getName() {
        return name;
    }
    
    @Override public boolean reachedPartiallyBy(Action a) {
        return condition.reachedPartiallyBy(a);
    }
    @Override public boolean reachedBy(Action a) {
        return condition.reachedBy(a);
    }
    @Override public boolean violatedBy(Action a) {
        return condition.violatedBy(a);
    }
    @Override public boolean violatedPartiallyBy(Action a) {
        return condition.violatedPartiallyBy(a);
    }
    @Override public String toString() {
        return "Goal" + (name.length() > 0? "(" + name + ")" : "") + " = { " + condition + ", Cost=" + failureCost + " };";
    }
    @Override public boolean check(State state) {
        return condition.check(state);
    }
    @Override public boolean check(State state, double theta) {
        return condition.check(state, theta);
    }
    @Override public List<String> getTargets() {
        return condition.getTargets();
    }
    @Override public List<Condition> getLeaves() {
        return condition.getLeaves();
    }
    @Override public boolean involvesConjuction() {
        return condition.involvesConjuction();
    }
    
    @Override public Condition clean() {
        return new Goal(condition.clean(), failureCost);
    }
    
    @Override public Condition reverse() {
        return new Goal(condition.reverse(), failureCost);
    }
}
