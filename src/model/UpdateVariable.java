
package model;
import distributions.Distribution;

/**
 *
 * @author Charline David 
 */
public class UpdateVariable extends Update {
    public static enum OP { 
        ASSIGN ("="), INCR ("+="), DECR ("-=");
        public final String str;
        OP(String str) { this.str = str; }
    };
    
    protected OP op;
    protected String value;
    
    public UpdateVariable(String v, OP op, String d) { 
        target = v;
        this.op = op;
        this.value = d;
    }
    
    @Override public State apply(State state) {
        Distribution v = state.getVariable(target).getValue();
        Distribution d = state.getVariable(value).getValue();
        return state.setVariable(new NumericVariable(target, 
            (op == OP.ASSIGN ? d : (op == OP.INCR ? v.add(d) : v.sub(d)))
        ));
    }
    @Override public State applyRelaxed(State state) {
        Distribution v = state.getVariable(target).getValue().toMeanValue();
        Distribution d = state.getVariable(value).getValue().toMeanValue();
        return state.setVariable(new NumericVariable(target, 
            (op == OP.ASSIGN ? d : (op == OP.INCR ? v.add(d) : v.sub(d)))
        ));
    }
    public String getValue() {
        return value;
    }
    
    @Override public String toString() {
        return "(" + op.str + " " + target + " " + value + ")";
    }
}
