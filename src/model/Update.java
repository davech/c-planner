
package model;

/**
 *
 * @author Charline David 
 */
public abstract class Update {
    protected String target;
    public String getTarget() {
        return target;
    }
    public abstract State apply(State state);
    public abstract State applyRelaxed(State state);
}