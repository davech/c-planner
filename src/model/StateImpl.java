
package model;

import java.util.*;

/**
 *
 * @author Charline David 
 */
public class StateImpl extends State {
    public StateImpl(ArrayList<String> f, HashMap<String,NumericVariable> v) {
        super(f, v);
    }
    /*public StateImpl(ArrayList<String> f, HashMap<String,NumericVariable> v, State w) {
        super(f, v, w);
    }*/
    public StateImpl(State s) {
        super(s);
    }
    public StateImpl(State.Data d) {
        super(d);
    }

    @Override protected String className() {
        return "State";
    }
    
    @Override public State apply(Action a) {
        if (!isApplicable(a)) {
            throw new UnsupportedOperationException("The following action is NOT APPLICABLE in the state:\n" + a + "\n" + toString());
        }
        State s = clone();
        ArrayList<Update> eff = a.getEffects();
        for (Update u : eff) {
            s = u.apply(s);
        }
        return s;
    }
}
