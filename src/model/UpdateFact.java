
package model;

/**
 *
 * @author Charline David 
 */
public class UpdateFact extends Update {
    protected boolean expected;
    
    public UpdateFact(String f, boolean exp) { 
        target = f;
        expected = exp;
    }
    
    public boolean getExpected() {
        return expected;
    }
    
    @Override public State apply(State state) {
        return state.setFact(target, expected);
    }
    @Override public State applyRelaxed(State state) {
        return (expected ? apply(state) : state.clone());
    }
    public boolean getValue() {
        return expected;
    }
    
    @Override public String toString() {
        return (expected? "" : "!") + target;
    }
}
