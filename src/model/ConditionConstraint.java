
package model;
import distributions.Distribution;
import java.util.*;

/**
 *
 * @author Charline David 
 */
public class ConditionConstraint extends Condition { 
    // w.v op l : op in {>,>=}
    public static double EPS = 0.0001;

    public static enum OP { 
        GT (">"), GE (">="), EQ ("=="), NEQ ("!="), LT ("<"), LE ("<=");
        public final String str;
        OP(String str) { this.str = str; }
    };
    
    protected String variable;
    protected OP op;
    protected double expected;
    
    /*public ConditionConstraint(NumericVariable v, OP o, double l) {
        variable = v.getKey();
        op = o;
        expected = l;
    }*/
    public ConditionConstraint(String v, OP o, double l) {
        variable = v;
        op = o;
        expected = l;
    }
    @Override public boolean check(State state) {
        return check(state, 0.5);
    }
    @Override public boolean check(State state, double theta) {
        NumericVariable nvar = state.getVariable(variable);
        if (nvar == null) {
            return false;
        }
        Distribution v = nvar.getValue();
        double result = v.evaluate(theta); //GT (">"), GE (">="), EQ ("=="), NEQ ("!="), LT ("<"), LE ("<=");
        return (op == OP.GT ? (result >  expected) : 
               (op == OP.GE ? (result >= expected) : 
               (op == OP.EQ ? (result >= expected - EPS) && (result <= expected + EPS) : 
               (op == OP.NEQ? (result < expected - EPS) || (result > expected + EPS) : 
               (op == OP.LT ? (result >  expected) : 
                              (result <= expected))))));
    }
    
    @Override public String toString() {
        return "(" + op.str + " " + variable + " "  + String.format("%.1f", expected) + ")";
    }
    
    @Override public List<String> getTargets() {
        List<String> list = new ArrayList<>();
        list.add(variable);
        return list;
    }
    
    @Override public List<Condition> getLeaves() {
        List<Condition> list = new ArrayList<>();
        list.add(this);
        return list;
    }
    
    @Override public boolean involvesConjuction() {
        return false;
    }
    
    @Override public boolean reachedBy(Action a) {
        // @TODO
        throw new UnsupportedOperationException("Not supported yet.");
    }
    @Override public boolean reachedPartiallyBy(Action a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override public boolean violatedBy(Action a) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    @Override public boolean violatedPartiallyBy(Action a) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override public Condition clean() {
        return this;
    }
    
    public Condition reverse() {
        OP[] values = OP.values();
        OP newOp = values[values.length - op.ordinal() - 1];
        return new ConditionConstraint(variable, newOp, expected);
    }
}
