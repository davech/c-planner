
package model;

import colesplanner.PlanningProblem;
import java.util.*;

/**
 *
 * @author Charline David 
 */
public class Action {
    public static class Data implements Cloneable {
        public String name;
        public Condition pre;
        public ArrayList<UpdateFact> effAdd; // a set of propositions
        public ArrayList<UpdateFact> effDel; // a set of propositions
        public ArrayList<UpdateVariable> effNum;
        public Data(String n) {
            name = n;
            effAdd = new ArrayList<>();
            effDel = new ArrayList<>();
            effNum = new ArrayList<>();
        }
        @Override public Data clone() {
            Data d = new Data(name);
            d.pre = pre;
            d.effAdd = effAdd;
            d.effDel = effDel;
            d.effNum = effNum;
            return d;
        }
    }
    
    public static Action endAction(Condition goals) {
        String name = "(end)";
        Condition pre;
        pre = goals;
        ArrayList<UpdateFact> eff = new ArrayList<>();

        return new Action(name, pre, eff);
    }
    public static Action s0Action(State s) {
        String name = "(s0)";
        Condition pre = new ConditionTrue();
        
        ArrayList<UpdateFact> eff = new ArrayList<>();
        for (String key : s.getFactKeys()) {
            eff.add(new UpdateFact(key, true));
        }
        ArrayList<UpdateVariable> effNum = new ArrayList<>();
        // @TODO effNum
        
        return new Action(name, pre, eff, effNum);
    }
    
    private Data data;
    
    public Action(String name, Condition pre, ArrayList<UpdateFact> effAdd, ArrayList<UpdateFact> effDel, ArrayList<UpdateVariable> effNum) {
        data = new Data(name);
        data.pre = pre;
        data.effAdd = effAdd;
        data.effDel = effDel;
        data.effNum = effNum;
    }
    public Action(String name, Condition pre, ArrayList<UpdateFact> eff, ArrayList<UpdateVariable> effNum) {
        data = new Data(name);
        data.pre = pre;
        data.effAdd = new ArrayList<>();
        data.effDel = new ArrayList<>();
        data.effNum = effNum;
        
        for (UpdateFact e : eff) {
            if (e.getExpected()) {
                data.effAdd.add(e);
            } else {
                data.effDel.add(e);
            }
        }
    }
    public Action(String name, Condition pre, ArrayList<UpdateFact> eff) {
        data = new Data(name);
        data.pre = pre;
        data.effAdd = new ArrayList<>();
        data.effDel = new ArrayList<>();
        data.effNum = new ArrayList<>();
        
        for (UpdateFact e : eff) {
            if (e.getExpected()) {
                data.effAdd.add(e);
            } else {
                data.effDel.add(e);
            }
        }
    }
    
    private boolean isApplicable(State state) {
        return data.pre.check(state);
    }
    
    public boolean hasNumericEffects() {
        return (!data.effNum.isEmpty());
    }
    
    public String getName() {
        return data.name;
    }
    public Condition getPre() {
        return data.pre;
    }
    public ArrayList<Update> getEffects() {
        ArrayList<Update> eff = new ArrayList<>();
        for (UpdateFact u : data.effAdd) {
            eff.add(u);
        }
        for (UpdateFact u : data.effDel) {
            eff.add(u);
        }
        for (UpdateVariable u : data.effNum) {
            eff.add(u);
        }
        return eff;
    }
    public Boolean getFactUpdateValue(String target) {
        for (UpdateFact u : data.effAdd) {
            if (u.getTarget().equals(target)) {
                return u.getValue();
            }
        }
        for (UpdateFact u : data.effDel) {
            if (u.getTarget().equals(target)) {
                return u.getValue();
            }
        }
        return null;
    }
    public ArrayList<String> effects() {
        ArrayList<String> eff = new ArrayList<>();
        for (Update u : getEffects()) {
            eff.add(u.getTarget());
        }
        return eff;
    }
    
    @Override public String toString() {
        String s = "Action" + data.name + " = {\n\tPre = { " + data.pre + " }";
        if (!data.effAdd.isEmpty()) {
            s += "\n\tEff+ = { ";
            for (UpdateFact u : data.effAdd) {
                s += u.toString() + "; ";
            }
            s += "}";
        }
        if (!data.effDel.isEmpty()) {
            s += "\n\tEff- = { ";
            for (UpdateFact u : data.effDel) {
                s += u.toString() + "; ";
            }
            s += "}";
        }
        if (!data.effNum.isEmpty()) {
            s += "\n\tEffNum = { ";
            for (UpdateVariable u : data.effNum) {
                s += u.toString() + "; ";
            }
            s += "}";
        }
        return s + "\n}";
    }

    private boolean couldBeApplicable(State baseState, Set<String> allModifiedEffects) {
        for (String c : data.pre.getTargets()) {
            String cond = c.substring(0, c.indexOf(' ')+1);
            if (cond.length() == 0) {
                cond = c;
            }
            if (!allModifiedEffects.contains(cond)) {
                if (!baseState.checkFact(c)) {
                    return false;
                }
            }
        }
        return true;
    }

    public Action asDynamic(State baseState, Set<String> allDynamicEffects) {
        if (isApplicable(baseState)) {
            return new Action(data.name, new ConditionTrue(), data.effAdd, data.effDel, data.effNum);
            
        } else if (couldBeApplicable(baseState, allDynamicEffects)) {
            Condition newPre = null;
            List<Condition> conds = data.pre.getLeaves();
            for (Condition c : conds) {
                String cond = c.getTargets().get(0);
                if (cond.indexOf(' ') >= 0) {
                    cond = cond.substring(0, cond.indexOf(' ')+1);
                }
                if (allDynamicEffects.contains(cond)) {
                    if (newPre == null) {
                        newPre = c;
                    } else {
                        newPre = new ConditionAnd(c, newPre);
                    }
                }
            }
            return new Action(data.name, newPre, data.effAdd, data.effDel, data.effNum);
        }
        throw new UnsupportedOperationException("The following action is NOT APPLICABLE in the baseState:\n" + toString() + "\nWorld" + PlanningProblem.worldState);
    }
    
    @Override public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof Action) {
            return (data.name.equals(((Action) o).data.name));
        }
        return false;
    }
}
