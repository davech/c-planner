
package colesplanner;

import java.util.*;
import model.*;

/**
 *
 * @author Charline David 
 */
public class ActionLayer {
    public ArrayList<Action> actions;
    
    public ActionLayer() {
        actions = new ArrayList<>();
    }
    public ActionLayer(Action a) {
        actions = new ArrayList<>();
        actions.add(a);
    }
    public ActionLayer(FactLayer fl, ArrayList<Action> allActions) {
        actions = new ArrayList<>();
        for (Action a : allActions) {
            if (fl.isApplicable(a)) {
                actions.add(a);
            }
        }
    }
    public ActionLayer(ArrayList<Action> actions) {
        this.actions = actions;
    }
    
    public int size() {
        return actions.size();
    }
    
    public ActionLayer diff(ActionLayer o) {
        ArrayList<Action> actionsSubset = new ArrayList<>();
        for (Action a : actions) {
            if (!o.actions.contains(a)) {
                actionsSubset.add(a);
            }
        }
        return new ActionLayer(actionsSubset);
    }
    public ActionLayer merge(ActionLayer o) {
        ArrayList<Action> actionsSuperset = new ArrayList<>();
        for (Action a : actions) {
            actionsSuperset.add(a);
        }
        for (Action a : o.actions) {
            if (!actionsSuperset.contains(a)) {
                actionsSuperset.add(a);
            }
        }
        return new ActionLayer(actionsSuperset);
    }
    
    public FactLayer applyAll(FactLayer fl) {
        for (Action a : actions) {
            fl = fl.apply(a);
        }
        return fl;
    }
    public List<Action> getAllValidActions(Condition c) {
        List<Action> allActions = new ArrayList<>();
        for (Action a : actions) {
            if (c.reachedBy(a)) {
                allActions.add(a);
            }
        }
        return allActions;
    }
    
    @Override public String toString() {
        String s = "ActionLayer = {";
        for (Action a : actions) {
            s += "\n\t" + a.getName() + ";";
        }
        return s + "\n}";
    }
}
