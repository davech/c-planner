package colesplanner;

/**
 * @author Charline David 
 */
public class Sample {
    private static class Data {
        public double costO  = 0, costB  = 0;
        public double failO  = 0, failB  = 0;
        public double nodesO = 0, nodesB = 0;
        public void add(Data d) {
            costO += d.costO;
            costB += d.costB;
            failO += d.failO;
            failB += d.failB;
            nodesO += d.nodesO;
            nodesB += d.nodesB;
        }
        public static String titles() {
            return (
                "Cost(O) \tCost(B) \t" + 
                "Fail(O) \tFail(B) \t" +
                "Size(O) \tSize(B)");
        }
        public String report() {
            return (
                String.format("%.1f",costO) + " \t" + 
                String.format("%.1f",costB) + " \t" + 
                String.format("%.0f",failO) + " \t" + 
                String.format("%.0f",failB) + " \t" +
                String.format("%.0f",nodesO) + " \t" + 
                String.format("%.0f",nodesB));
        }
        public static String csvTitles() {
            return titles().replace(" \t",",") + ",";
        }
        public String csv() {
            return report().replace(" \t",",") + ",";
        }
    }
    
    private String name;
    private double fullCost;
    private int nbRuns = 0;
    private StringBuilder contents;
    private Data data, currentData;
    
    public Sample(String n, double c) {
        name = n;
        fullCost = c;
        contents = new StringBuilder();
        data = new Data();
        currentData = new Data();
        contents.append(data.csvTitles());
    }
    
    public static String titles() {
        return ("Problem \t" + Data.titles() + " \tRuns");
    }
    public String report() {
        double runs = (double)nbRuns;
        return (name + " \t" +
            String.format("%.1f",data.costO/(runs-data.failO)) + " \t" + 
            String.format("%.1f",data.costB/(runs-data.failB)) + " \t" + 
            String.format("%.2f",(100.0*data.failO)/runs) + " \t" + 
            String.format("%.2f",(100.0*data.failB)/runs) + " \t" +
            String.format("%.0f",(data.nodesO)/runs) + " \t" + 
            String.format("%.0f",(data.nodesB)/runs) + " \t" + 
            nbRuns + " \t/" + String.format("%.1f",fullCost));
    }
    public String plainData() {
        return (name + " \t" + data.report() + " \t" + 
            nbRuns + " \t/" + String.format("%.1f",fullCost));
    }
    public static String csvTitles() {
        String s = titles();
        return s.replace(" \t",",");
    }
    public String csv() {
        //String s = plainData();
        //return s.replace(" \t",",");
        return contents.toString();
    }
    
    public String problemName() {
        return name;
    }
    public int nbRuns() {
        return nbRuns;
    }

    void incrRuns() { nbRuns += 1; }
    void incrSizeO(int size) { currentData.nodesO += size; }
    void incrSizeB(int size) { currentData.nodesB += size; }
    void incrFailO() { currentData.failO += 1; }
    void incrFailB() { currentData.failB += 1; }
    void incrCostO(double cost) { currentData.costO += cost; }
    void incrCostB(double cost) { currentData.costB += cost; }

    void next() {
        contents.append("\n").append(currentData.csv());
        data.add(currentData);
        currentData = new Data();
    }
}
