
package colesplanner;

import java.util.*;
import model.*;
import graphs.Pair;
import graphs.TreeNode;
import java.util.concurrent.*;
/**
 *
 * @author Charline David 
 */
public class PlanningProblem {
    public static State worldState;
    private State initialState;
    private ArrayList<Goal> goals;
    private ArrayList<Action> actions;
    private Condition globalConditions;
    private double confidenceLevel;
    //private RPGHeuristic heuristic;
    private long timeLimit = Utility.Ti;
    private boolean verbose;
    
    public PlanningProblem(State i, ArrayList<Goal> g, ArrayList<Action> a, Condition c, double theta) { //, ArrayList<Condition> c
        verbose =  false;
        initialState = i;
        goals = g;
        actions = a;
        globalConditions = c;
        assert(theta < 1 && 0.5 <= theta);
        confidenceLevel = theta;
    }
    
    public void reset() {
        timeLimit = Utility.Ti;
    }
    public void close() {
        worldState = null;
    }
    
    /*public State initialState() {
        return initialState;
    }*/
    
    /*public ArrayList<Goal> goals() {
        return goals;
    }*/
    
    public double goalsCost() {
        double cost = 0;
        for (Goal g : goals) {
            cost += g.getCost();
        }
        return cost;
    }
    
    public void setVerbose(boolean v) {
        verbose = v;
    }
    
    /**
     * Setup step 
     */
    public void setWorldState(State s) {
        worldState = s;
    }
    public void setInitialState(State s) {
        initialState = s;
    }
    
    public void ready(){
        // Separate world facts from the initial state
        if (worldState != null) {
            return;
        }
        State.InitialStateBuilder builder = new State.InitialStateBuilder(initialState, actions);
        
        initialState = builder.getInitialState();
        worldState = builder.getWorldState();
        actions = builder.getCleanActions();
    
        if (verbose) {
            System.out.println("World" + worldState);
            System.out.println("Initial" + initialState);
            System.out.println("Down to " + actions.size() + " actions.");
            for (Goal g : goals) {
                System.out.println(g);
            }
            /*for (Action a : actions) {
                System.out.println(a);
            }*/
            System.out.println();
        }
        System.out.println("Time for linear plan: " + Utility.Ti + "sec.; Time for branch planners: " + Utility.Tb + "sec.");
    }
    
    /**
     * Planning step
     */
    public TreeNode<Policy> justPlan() {
        return toPolicyPlan(plan(initialState, Double.POSITIVE_INFINITY));
    }
    
    public TreeNode<Policy> branchPlan() {
        TreeNode<Policy> branchPlan = branchPlan(initialState, Double.POSITIVE_INFINITY);
        cleanFinalSolution(branchPlan);
        return branchPlan;
    }
    
    private TreeNode<Policy> branchPlan(State state, double costBound) {
        TreeNode<Policy> tree = toPolicyPlan(plan(state, costBound));
        
        if (tree == null) {
            return null;
        }
        
        double cost1 = Utility.goalCost(Utility.unrollBestPolicyPlan(state, tree), goals);
        State state1 = state.clone();
        TreeNode<Policy> i = tree;
        
        while (i.childrenCount() > 0) {
            Action ai = i.data().action();
            state1 = state1.apply(ai);
            //if (ai.hasNumericEffects()) {
                State state2 = state1.toMeanValue();
                TreeNode<Policy> tree1 = branchPlan(state2, cost1);
                /*if (tree1 != null) {
                    cost1 = Utility.goalCost(Utility.unrollBestPolicyPlan(state1, tree1), goals);
                }*/
                if (tree1 != null) {
                    i.getChild(0).mergeIn(tree1);
                }
            //}
            i = i.getChild(0);
        }
        labelTree(tree, state);
        return tree;
    }
    
    private void labelTree(TreeNode<Policy> root, State state) {
        State s1 = state.apply(root.data().action());
        State s0 = s1.toMeanValue();
        
        for (int i = 0; i < root.childrenCount(); i++) {
            TreeNode<Policy> j = root.getChild(i);
            labelTree(j, s1);
        }
        if (root.childrenCount() > 1) {
            for (int i = 0; i < root.childrenCount(); i++) {
                TreeNode<Policy> j = root.getChild(i);
                List<TreeNode<Policy>> branches = j.getChildBranches();
                for (TreeNode<Policy> branch : branches) {
                    State sg = Utility.unrollPolicyPlan(s0, branch);
                    double energy = Utility.energyCost(s0, sg, confidenceLevel);
                    double cost = Utility.goalCost(sg, goals);
                    j.data().addPolicy(energy, cost);
                }
            }
        }
    }
    
    private void cleanFinalSolution(TreeNode<Policy> root) {
        if (root == null) {
            return;
        }
        int count = root.childrenCount();
        if (count > 1 && count == root.leavesCount()) {
            // changes policy condition to (energy < child(1).energy)
            Policy child0 = root.getChild(0).data();
            Double cost0 = child0.getPolicy(0).second();
            child0.clearPolicies();
            child0.addPolicy(new ConditionTrue(), cost0);
        }
        for (int i = 0; i < count; i++) {
            cleanFinalSolution(root.getChild(i));
        }
    }
    
    private TreeNode<Action> plan(State state, double costBound) {
        // ecai10.pdf
        TreeNode<Action> end = new TreeNode<>(Action.endAction(new ConditionTrue()));
        if (state == null) {
            return end;
        }
        TreeNode<Action> plan = null;
        RPGHeuristic heur = new RPGHeuristic(goals, globalConditions, actions, confidenceLevel, state, costBound);
        final ExecutorService service = Executors.newSingleThreadExecutor();
        try {
            service.submit(heur).get(timeLimit, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
        } catch (Exception e) {
            System.out.println(e);
            for (StackTraceElement trace : e.getStackTrace()) {
                System.out.println(trace);
            }
            throw new RuntimeException(e); 
        } finally {
            service.shutdownNow();
            plan = heur.get();
            if (plan != null) {
                plan.getLeaves().get(0).addChild(end);
            }
            timeLimit = Utility.Tb;
            return plan;
        }
    }
    private TreeNode<Policy> toPolicyPlan(TreeNode<Action> actionPlan) {
        if (actionPlan == null) {
            return null;
        }
        TreeNode<Policy> root = new TreeNode<>(new Policy(actionPlan.data()));
        for (int i = 0; i < actionPlan.childrenCount(); i++) {
            TreeNode<Action> j = actionPlan.getChild(i);
            root.addChild(toPolicyPlan(j));
        }
        return root;
    }

    public void simulate(TreeNode<Policy> planO, Sample sample) {
        if (planO == null) {
            return;
        }
        
        sample.incrRuns();
        sample.incrSizeO(planO.leftmostHeight() - 1);
        sample.incrSizeB(planO.size() - planO.leavesCount());

        State sO = initialState.apply(planO.data().action())
                               .simulate();
        TreeNode<Policy> planB = null;
        State sB = null;
        
        // Simulate original linear plan
        while(planO.hasChildren()) {
            if (sB == null && planO.childrenCount() > 1) {
                sB = sO;
                planB = planO;
            }
            planO = planO.getChild(0);
            sO = sO.apply(planO.data().action())
                   .simulate();
        }
        if (!globalConditions.check(sO)) {
            sample.incrFailO();
        } else {
            sample.incrCostO(Utility.goalCost(sO, goals));
        }
        
        // Simulate branching plan
        if (sB == null || planB == null) {
            if (!globalConditions.check(sO)) {
                sample.incrFailB();
            } else {
                sample.incrCostB(Utility.goalCost(sO, goals));
            }
        } else {
            while(planB.hasChildren()) {
                TreeNode<Policy> bestChild = planB.getChild(0);
                double bestCost = Double.POSITIVE_INFINITY;
                for (TreeNode<Policy> child : planB.children()) {
                    for (Pair<Condition,Double> policy : child.data().getPolicies()) {
                        if (policy.first().check(sB) && policy.second() < bestCost) {
                            bestCost = policy.second();
                            bestChild = child;
                        }
                    }
                }
                planB = bestChild;
                sB = sB.apply(planB.data().action())
                       .simulate();
            }
            if (!globalConditions.check(sB)) {
                sample.incrFailB();
            } else {
                sample.incrCostB(Utility.goalCost(sB, goals));
            }
        }
        
        sample.next();
    }
}
