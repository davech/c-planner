package colesplanner;

import model.*;
import distributions.Distribution;
import graphs.*;
import java.util.*;

/**
 *
 * @author Charline David 
 */
public class Utility {
    public static long Ti = 60;
    public static long Tb = 10;
    
    public static State unrollPlan(State s0, graphs.TreeNode<Action> tree) {
        if (tree.data() == null) {
            return s0;
        }
        State sf = s0.apply(tree.data());
        while(tree.hasChildren()) {
            tree = tree.getChild(0);
            sf = sf.apply(tree.data());
        }
        return sf;
    }
    public static State unrollPolicyPlan(State s0, graphs.TreeNode<Policy> tree) {
        if (tree.data() == null) {
            return s0;
        }
        State sf = s0.apply(tree.data().action());
        while(tree.hasChildren()) {
            tree = tree.getChild(0);
            sf = sf.apply(tree.data().action());
        }
        return sf;
    }
    
    public static State unrollBestPolicyPlan(State s0, TreeNode<Policy> tree) {
        if (tree.data() == null) {
            return s0;
        }
        TreeNode<Policy> t = tree;
        State sf = s0.apply(t.data().action());
        while(t.hasChildren()) {
            t = t.getChild(t.childrenCount() - 1);
            sf = sf.apply(t.data().action());
        }
        return sf;
    }
    
    public static double goalCost(State fl, ArrayList<Goal> goals) {
        double cost = 0;
        for (Goal g : goals) {
            cost += g.checkCost(fl);
        }
        return cost;
    }
    public static double energyCost(State s0, State sg, double theta) {
        double m = 1;
        for (String v : sg.getVariableKeys()) {
            Distribution finalV = sg.getVariable(v).getValue();
            Distribution initV = s0.getVariable(v).getValue();
            m *= (initV.sub(finalV)).evaluate(theta);
        }
        return m;
    }
}
