
package colesplanner;

import model.*;
import graphs.*;
import java.util.*;

/**
 *
 * @author Charline David 
 */
public class RPGHeuristic implements Runnable{

    
    public static class Layers {
        public ArrayList<ActionLayer> al;
        public ArrayList<FactLayer> fl;
        public Layers(ArrayList<ActionLayer> al, ArrayList<FactLayer> fl) {
            this.al = al;
            this.fl = fl;
        }
        public Layers(Layers l) {
            this.al = (ArrayList<ActionLayer>)l.al.clone();
            this.fl = (ArrayList<FactLayer>)l.fl.clone();
        }
    }
    // https://aaai.org/ocs/index.php/ICAPS/ICAPS11/paper/view/2668/3132
    private ArrayList<Action> actions;
    private ArrayList<Goal> atEndPreferences; // goals
    private Condition alwaysPreferences; // globalConditions
    private double confidenceLevel;
    
    private State theState;
    private double theCostBound;
    private TreeNode<Action> theBestPlan;
    
    public RPGHeuristic(ArrayList<Goal> g, Condition c, ArrayList<Action> a, double theta, State s, double cost) {
        atEndPreferences = g;
        alwaysPreferences = c;
        actions = a;
        confidenceLevel = theta;
        theState = s;
        theCostBound = cost;
    }
    
    public TreeNode<Action> get() {
        return theBestPlan;
    }
    
    @Override
    public void run() {
        double bestCost = theCostBound;
        double bestEnergy = 0;
        theBestPlan = null;
        
        while (true) {
            TreeNode<Action> plan = evaluate(theState);
            if (plan.hasChildren()) {
                State sg = Utility.unrollPlan(theState, plan);
                double cost = Utility.goalCost(sg, atEndPreferences);
                if (cost <= bestCost) {
                    double energy = Utility.energyCost(theState, sg, confidenceLevel);
                    if (cost < bestCost || energy < bestEnergy) {
                        bestCost = cost;
                        theBestPlan = plan.getChild(0);
                        bestEnergy = energy;
                    }
                }
            }
        }
    }
    
    public TreeNode<Action> evaluate(State state) {
        double bestCost = Utility.goalCost(state, atEndPreferences);
        if (!alwaysPreferences.check(state, confidenceLevel)) {
            return null;
        } else if (bestCost == 0) {
            return new TreeNode(Action.s0Action(state));
        }
        
        TreeNode<Action> relaxedRoot = relaxedPlan(state);
        TreeNode<Action> child = null;
        
        for(int i = 0; i < relaxedRoot.childrenCount(); i++) {
            TreeNode<Action> c = relaxedRoot.getChild(i);
            Action a = c.data();
            try {
                TreeNode<Action> c2 = evaluate(state.apply(a));
                if (c2 != null) {
                    c2.setData(a);
                    
                    State sf = Utility.unrollPlan(state, c2);
                    double cost = Utility.goalCost(sf, atEndPreferences);
                    if (cost < bestCost) {
                        bestCost = cost;
                        child = c2;
                    }
                }
            } catch (UnsupportedOperationException e) {}
        }
        
        TreeNode<Action> realRoot = new TreeNode(relaxedRoot.data());
        if (child != null) {
            realRoot.addChild(child);
        }
        
        return realRoot;
    }
    
    public TreeNode<Action> relaxedPlan(State state) {
        Layers layers = makeLayers(state);
        TreeNode<Action> relaxedTree = relaxedTree(new Layers(layers)).reverse();
        
        TreeNode<Action> relaxedPlan = new TreeNode<>(relaxedTree.data());
        TreeNode<Action> p = relaxedPlan;
        TreeNode<Action> t = relaxedTree;
        while (t.hasChildren()) {
            t = t.getChild((int)(Math.random()*t.childrenCount()));
            TreeNode<Action> pc = new TreeNode<>(t.data());
            p.addChild(pc);
            p = pc;
        }
        
        return relaxedPlan;
    }
    
    private Layers makeLayers(State state) {
        ArrayList<ActionLayer> actionLayers = new ArrayList<>();
        ArrayList<FactLayer> factLayers = new ArrayList<>();
        
        // fl[0] et al[0]
        FactLayer fl = new FactLayer(state);
        ActionLayer al = new ActionLayer(fl, actions);
        ActionLayer prev_al = new ActionLayer();
        factLayers.add(fl);
        
        while(Utility.goalCost(fl, atEndPreferences) > 0 && prev_al.size() != al.size()) {
            actionLayers.add(al);
            
            prev_al = al;
            fl = new FactLayer(al.applyAll(fl));
            al = new ActionLayer(fl, actions);
            factLayers.add(fl);
        }
        
        assert(actionLayers.size() + 1 == factLayers.size());
        return new Layers(actionLayers, factLayers);
    }
    
    private TreeNode<Action> relaxedTree(Layers layers) {
        Action endAction = endAction();
        Action s0Action = Action.s0Action(layers.fl.get(0));
        TreeNode<Pair<Action,Condition>> relaxedPlan = new TreeNode(new Pair<>(endAction,endAction.getPre()));

        ActionLayer a0 = new ActionLayer(s0Action);
        
        Collections.reverse(layers.fl);
        Collections.reverse(layers.al);
        layers.fl.add(new FactLayer());
        layers.al.add(a0);
        layers.al.add(new ActionLayer());
        
        for (int i = 0; i < layers.al.size()-1; i++) {
            for(TreeNode<Pair<Action,Condition>> node : relaxedPlan.getLeaves()) {
                if (!node.data().first().equals(s0Action)) {
                    ActionLayer al = (layers.al.get(i).diff(layers.al.get(i+1))).merge(a0);
                    FactLayer fl = layers.fl.get(i).diff(layers.fl.get(i+1));

                    Condition p = node.data().second();
                    //ask fl what condition it can/can't fullfill <- p1, p0
                    Condition p0 = fl.cantFulfillCondition(p);
                    Condition p1 = fl.canFulfillCondition(p);

                    if (p1 != null) {
                        for (Condition sub_p : p1.getLeaves()) {
                            List<Action> acts = al.getAllValidActions(sub_p);
                            Action a = acts.get((int)(Math.random()*acts.size()));

                            Condition g = (p0 == null? a.getPre() : new ConditionAnd(p0, a.getPre()));
                            node.addChild(new Pair<>(a, g.clean()));
                        }
                    }
                }
            }
        }
        TreeNode<Action> actionTree = convertToActionTree(relaxedPlan);
        return actionTree;
    }
    
    private TreeNode<Action> convertToActionTree(TreeNode<Pair<Action,Condition>> root) {
        TreeNode<Action> plan = new TreeNode<>(root.data().first());
        for(int i = 0; i < root.childrenCount(); i++) {
            TreeNode<Action> c = convertToActionTree(root.getChild(i));
            plan.addChild(c);
        }
        return plan;
    }
    
    private Action endAction() {
        Condition pre = conditionAndGoals();
        return Action.endAction(pre);
    }

    private Condition conditionAndGoals() {
        Condition goals = atEndPreferences.get(0).toCondition();
        for (Goal g : atEndPreferences.subList(1, atEndPreferences.size())) {
            goals = new ConditionAnd(g.toCondition(), goals);
        }
        return goals;
    }
    
}
