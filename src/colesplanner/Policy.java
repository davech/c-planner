
package colesplanner;

import model.*;
import graphs.Pair;
import java.util.*;

/**
 *
 * @author Charline David 
 */
public class Policy {
    private Action action;
    private List<Pair<Condition,Double>> policies; // energy condition, cost
    
    public Policy(Action a) {
        action = a;
        policies = new ArrayList<>();
    }
    
    public Action action() {
        return action;
    }
    public void addPolicy(double e, double cost) {
        Condition cond = new ConditionConstraint("(energy rover0)", ConditionConstraint.OP.GE, e);
        policies.add(new Pair<>(cond, cost));
    }
    public void addPolicy(Condition cond, double cost) {
        policies.add(new Pair<>(cond, cost));
    }
    public Pair<Condition,Double> getPolicy(int i) {
        if (i < 0 || i >= policies.size()) {
            return null;
        }
        return policies.get(i);
    }
    public List<Pair<Condition,Double>> getPolicies() {
        return policies;
    }
    
    @Override public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (o instanceof Policy) {
            Policy p = (Policy)o;
            if (!action.equals(p.action) || policies.size() != p.policies.size()) {
                return false;
            }
            for (int i = 0; i < policies.size(); i++) {
                if (!policies.contains(p.policies.get(i))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public void clearPolicies() {
        policies.clear();
    }
    
    @Override public String toString() {
        String s = action.getName();
        String p = "";
        if (!policies.isEmpty()) {
            p += policies.get(0).toString();
            for (Pair<Condition,Double> policy : policies.subList(1, policies.size())) {
                p += "," + policy.toString();
            }
            p += ":\n";
        }
        return p + s;
    }
}
