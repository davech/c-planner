
package colesplanner;

import java.util.*;
import model.*;

/**
 *
 * @author Charline David 
 */
public class FactLayer extends State {
    public FactLayer() {
        super();
    }
    public FactLayer(ArrayList<String> f, HashMap<String,NumericVariable> v) {
        super(f, v);
    }
    public FactLayer(State s) {
        super(s);
    }
    public FactLayer(State.Data d) {
        super(d);
    }
    
    @Override protected String className() {
        return "FactLayer";
    }
    
    @Override public FactLayer apply(Action a) {
        State s = clone();
        if (!isApplicable(a)) {
            throw new UnsupportedOperationException("The following action is NOT APPLICABLE in the state:\n" + a + "\n" + toString());
        }
        ArrayList<Update> eff = a.getEffects();
        for (Update u : eff) {
            s = u.applyRelaxed(s);
        }
        return (FactLayer) s;
    }
    
    public Condition canFulfillCondition(Condition p) {
        List<Condition> conds = p.getLeaves();
        ArrayList<Condition> res = new ArrayList<>();
        for (Condition c : conds) {
            if (c.check(this)) {
                res.add(c);
            }
        }
        if (res.isEmpty()) {
            return null;
        } else {
            Condition ret = res.get(0);
            for (Condition c : res.subList(0, res.size())) {
                ret = new ConditionAnd(c, ret);
            }
            return ret;
        }
    }
    public Condition cantFulfillCondition(Condition p) {
        List<Condition> conds = p.getLeaves();
        ArrayList<Condition> res = new ArrayList<>();
        for (Condition c : conds) {
            if (!c.check(this)) {
                res.add(c);
            }
        }
        if (res.isEmpty()) {
            return null;
        } else {
            Condition ret = res.get(0);
            for (Condition c : res.subList(0, res.size())) {
                ret = new ConditionAnd(c, ret);
            }
            return ret;
        }
    }
    
    public FactLayer diff(FactLayer o) {
        ArrayList<String> factsSubset = new ArrayList<>();
        HashMap<String,NumericVariable> variablesSubset = new HashMap<>();
        for (String k : data.facts) {
            if (!o.data.facts.contains(k)) {
                factsSubset.add(k);
            }
        }
        for (String k : data.variables.keySet()) {
            if (!o.data.variables.keySet().contains(k)) {
                variablesSubset.put(k, data.variables.get(k));
            }
        }
        return new FactLayer(factsSubset, variablesSubset);
    }
}
