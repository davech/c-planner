
package distributions;

import static org.apache.commons.math3.special.Erf.erfcInv;

/**
 *
 * @author Charline David 
 */
class Normal implements InternalDistribution {
    private double mean;
    private double var;
    private double sqrt_2var;

    public Normal() {
        mean = 0;
        var = 0;
        sqrt_2var = 0;
    }
    public Normal(Normal n) {
        mean = n.mean;
        var = n.var;
        sqrt_2var = n.sqrt_2var;
    }
    public Normal(double m, double v) {
        mean = m;
        var = v;
        sqrt_2var = Math.sqrt(2*v);
    }
    public Normal(Constant c) {
        mean = c.getMean();
        var = 0;
        sqrt_2var = 0;
    }
    
    @Override public InternalDistribution add(InternalDistribution d) {
        if (d instanceof Constant) {
            return new Normal(mean + d.getMean(), var);
        } else if (d instanceof Normal) {
            return new Normal(mean + d.getMean(), var + d.getVariance());
        }
        throw new UnsupportedOperationException("Not supported yet.");
    }
    @Override public InternalDistribution sub(InternalDistribution d) {
        return add(d.inverse());
    }
    @Override public InternalDistribution inverse() {
        return new Normal(-mean, var);
    }

    @Override public InternalDistribution clone() {
        return new Normal(this);
    }

    @Override public double getMean() {
        return mean;
    }

    @Override public double getVariance() {
        return var;
    }

    @Override public String toString() {
        return "N(" + mean + "," + var + ")";
    }
    @Override public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o instanceof InternalDistribution) {
            if (isConstant()) {
                return (((InternalDistribution) o).isConstant() && 
                        this.toConstant().equals(((InternalDistribution) o).toConstant()));
            } else if (o instanceof Normal) {
                return (mean == ((Normal) o).mean) && (var == ((Normal) o).var);
            }
        }
        return false;
    }

    @Override public boolean isConstant() {
        return (var == 0);
    }
    @Override public boolean isEmpty() {
        return equals(new Normal());
    }

    @Override public InternalDistribution toConstant() {
        return new Constant(this);
    }
    @Override public InternalDistribution toNormal() {
        return new Normal(this);
    }

    @Override public double evaluate(double theta) {
        if (var == 0 || theta == 0.5) {
            return mean;
        }
        return (sqrt_2var * erfcInv(2 * theta) + mean);
    }
    
    @Override public double sample() {
        return mean + RANDOM.nextGaussian() * Math.sqrt(var);
    }
}
