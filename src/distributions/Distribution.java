package distributions;

/**
 *
 * @author Charline David 
 */
public class Distribution {
    private InternalDistribution state;
    
    public Distribution() {
        state = new Constant(0);
    }
    public Distribution(double d) {
        state = new Constant(d);
    }
    public Distribution(String dist, double d) {
        state = new Constant(d);
    }
    public Distribution(String dist, double d1, double d2) {
        state = new Normal(d1, d2);
        if (state.isConstant()) {
            state = state.toConstant();
        }
    }
    public Distribution(Distribution d) {
        state = d.state.clone();
    }
    public Distribution(InternalDistribution d) {
        state = d.clone();
    }
    public Distribution add(Distribution d2) {
        return new Distribution(state.add(d2.state));
    }
    public Distribution sub(Distribution d2) {
        return new Distribution(state.sub(d2.state));
    }
    
    public Distribution toMeanValue() {
        return new Distribution(state.toConstant());
    }
    
    @Override public String toString() {
        return state.toString();
    }
    
    @Override public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (o instanceof Distribution) {
            return (state.equals(((Distribution) o).state));
        }
        return false;
    }
    
    public Distribution clone() {
        return new Distribution(this);
    }
    
    public double evaluate(double theta) {
        return state.evaluate(theta);
    }
    public double mean() {
        return state.getMean();
    }
    public Distribution sample() {
        return new Distribution(state.sample());
    }
}
