
package distributions;

import java.util.Comparator;
import java.util.Random;

/**
 *
 * @author Charline David 
 */
public interface InternalDistribution {
    public static Random RANDOM = new Random();
    
    public static class DComparator implements Comparator<InternalDistribution> {
        public int compare(InternalDistribution a, InternalDistribution b) {
            double m = a.getMean() - b.getMean();
            if (m > 0) {
                return 1;
            } else if (m < 0) {
                return -1;
            }
            return 0;
        }
    }
    public static class DComparator2 implements Comparator<InternalDistribution> {
        public int compare(InternalDistribution a, InternalDistribution b) {
            double m = a.getMean() - b.getMean();
            double v = a.getVariance() - b.getVariance();
            if (m > 0 || (m == 0 && v > 0)) {
                return 1;
            } else if (m < 0 || (m == 0 && v < 0)) {
                return -1;
            }
            return 0;
        }
    }

    public InternalDistribution add(InternalDistribution d);
    public InternalDistribution sub(InternalDistribution d);
    public InternalDistribution inverse();
    public InternalDistribution clone();
    public double getMean();
    public double getVariance();
    public double evaluate(double theta);
    public boolean isConstant();
    public boolean isEmpty();
    public InternalDistribution toConstant();
    public InternalDistribution toNormal();
    public double sample();
    
}
