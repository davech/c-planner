
package distributions;

/**
 *
 * @author Charline David 
 */
class Constant implements InternalDistribution {
    private double value;
    
    public Constant()           { value = 0; }
    public Constant(double v)   { value = v; }
    public Constant(Constant c) { value = c.value; }
    public Constant(Normal n)   { value = n.getMean(); }
    //public Constant(InternalDistribution d) { value = d.getMean(); }
    
    @Override public InternalDistribution clone() {
        return new Constant(this);
    }
    
    @Override public InternalDistribution add(InternalDistribution d) {
        if (d instanceof Constant) {
            return new Constant(value + d.getMean());
        } else if (d instanceof Normal) {
            return new Normal(value + d.getMean(), d.getVariance());
        }
        throw new UnsupportedOperationException("Not supported yet.");
    }
    @Override public InternalDistribution sub(InternalDistribution d) {
        return add(d.inverse());
    }
    @Override public InternalDistribution inverse() {
        return new Constant(-value);
    }

    @Override public double getMean() {
        return value;
    }

    @Override public double getVariance() {
        return 0.0;
    }
    
    @Override public String toString() {
        return "" + value;
    }
    @Override public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o instanceof InternalDistribution) {
            if (((InternalDistribution) o).isConstant()) {
                return (value == ((InternalDistribution) o).getMean());
            }
        }
        return false;
    }

    @Override public boolean isConstant() {
        return true;
    }
    @Override public boolean isEmpty() {
        return equals(new Constant());
    }
    @Override public InternalDistribution toConstant() {
        return new Constant(this);
    }
    @Override public InternalDistribution toNormal() {
        return new Normal(this);
    }
    
    @Override public double evaluate(double theta) {
        return value;
    }
    
    @Override public double sample() {
        return value;
    }
}
