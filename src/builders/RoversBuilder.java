
package builders;

import model.*;
import distributions.Distribution;
import java.io.*;
import java.util.*;

/**
 *
 * @author Charline David 
 */
public class RoversBuilder extends Builder {
    String init, goal, metric, objects;
    
    public RoversBuilder(String filename) {
        verbose = false;
        init(filename);
    }
    public RoversBuilder(String filename, boolean v) {
        verbose = v;
        init(filename);
    }
    
    @Override
    public State initialState() {
        List<String> args = split(init);
        
        ArrayList<String> f = new ArrayList<>();
        HashMap<String,NumericVariable> v = new HashMap<>();
        
        double energy = 0;
        for (String arg : args) {
            if (!arg.contains("rover1"))  {
                if (arg.startsWith("(= ")) { // numeric variable
                    String key = split(arg).get(0);
                    double value = Double.parseDouble(arg.substring(arg.lastIndexOf(" ")+1, arg.length()-1));
                    Distribution traverse_cost = new Distribution("Normal", value, value); //1.7 & 1.0
                    energy += value;

                    v.put(key, new NumericVariable(key, traverse_cost));

                } else { // fact
                    f.add(arg);
                }
            }
        }
        //0.98 (cost=0), 0.975 (cost=76.6->0.0), 0.85 (cost=116.0->76.5), 0.65 (cost=293.9->254.4 FAILURE)
        v.put("(energy rover0)", new NumericVariable("(energy rover0)", energy*0.6)); //1.41 (cost=116.0->76.5)
        
        State initialState = new StateImpl(f, v);
        return initialState;
    }
    
    @Override
    public ArrayList<Goal> goals() {
        List<String> goal_args = split(split(goal).get(0));
        List<String> metric_args = split(split(metric).get(0));
        
        HashMap<String,Double> costs = new HashMap<>();
        for (String arg : metric_args) {
            String is_violated = split(arg).get(0);
            String name = is_violated.substring(is_violated.indexOf(" ")+1, is_violated.length()-1);
            Double cost = Double.parseDouble(arg.substring(arg.lastIndexOf(" ")+1, arg.length()-1));
            
            costs.put(name, cost);
        }
        
        ArrayList<Goal> goals = new ArrayList<>();
        for (String arg : goal_args) {
            Condition condition;
            String condstr = split(arg).get(0);
            if (condstr.startsWith("(and ")) {
                List<String> conditions = split(condstr);
                condition = new ConditionAssignment(conditions.get(0));
                for (String c : conditions.subList(1, conditions.size())) {
                    condition = new ConditionAnd(new ConditionAssignment(c), condition);
                }
            } else {
                condition = new ConditionAssignment(condstr);
            }
            String name = arg.substring(arg.indexOf(" ")+1, arg.indexOf(" ",arg.indexOf(" ")+1));
            
            goals.add(new Goal(name, condition, costs.get(name)));
        }
        
        if (verbose) {
            for (Goal g : goals) {
                System.out.println(g);
            }
        }
        return goals;
    }
    
    @Override
    public ArrayList<Action> actions() {
        String[] args = objects.split(" - ");
        
        List<String> landers    = new ArrayList<>();
        List<String> modes      = new ArrayList<>();
        List<String> rovers     = Arrays.asList(new String[]{"rover0"});
        List<String> stores     = new ArrayList<>();
        List<String> waypoints  = new ArrayList<>();
        List<String> cameras    = new ArrayList<>();
        List<String> objectives = new ArrayList<>();
        
        for (int i = 0; i < args.length-1; i++) {
            String[] instances = args[i].split(" ");
            String typeName = args[i+1].split(" ")[0].replaceAll("\\)", "");
            for (int j = 1; j < instances.length; j++) {
                if (typeName.equals("lander")) {
                    landers.add(instances[j]);
                } else if (typeName.equals("mode")) {
                    modes.add(instances[j]);
                } else if (typeName.equals("rover")) {
                    //rovers.add(instances[j]);
                } else if (typeName.equals("store")) {
                    stores.add(instances[j]);
                } else if (typeName.equals("waypoint")) {
                    waypoints.add(instances[j]);
                } else if (typeName.equals("camera")) {
                    cameras.add(instances[j]);
                } else if (typeName.equals("objective")) {
                    objectives.add(instances[j]);
                }
            }
        }
        
        if (verbose) {
            System.out.print("" + landers.size() + " Landers:");
            for (String s : landers) System.out.print(" " + s);
            System.out.print(".\n" + modes.size() + " Modes:");
            for (String s : modes) System.out.print(" " + s);
            System.out.print(".\n" + rovers.size() + " Rovers:");
            for (String s : rovers) System.out.print(" " + s);
            System.out.print(".\n" + stores.size() + " Stores:");
            for (String s : stores) System.out.print(" " + s);
            System.out.print(".\n" + waypoints.size() + " Waypoints:");
            for (String s : waypoints) System.out.print(" " + s);
            System.out.print(".\n" + cameras.size() + " Cameras:");
            for (String s : cameras) System.out.print(" " + s);
            System.out.print(".\n" + objectives.size() + " Objectives:");
            for (String s : objectives) System.out.print(" " + s);
            System.out.println(".");
        }
        
        ArrayList<Action> actions = new ArrayList<>();
        
        // NAVIGATE
        for (String x : rovers) {
            for (String y : waypoints) {
                for (String z : waypoints) {
                    String name = "(navigate "+x+" "+y+" "+z+")";
                    
                    Condition c0 = new ConditionAssignment("(can_traverse "+x+" "+y+" "+z+")");
                    Condition c1 = new ConditionAssignment("(available "+x+")");
                    Condition c2 = new ConditionAssignment("(at "+x+" "+y+")");
                    Condition c3 = new ConditionAssignment("(visible "+y+" "+z+")");
                    Condition pre = new ConditionAnd(c0, c1, c2, c3);
                    
                    UpdateFact e0 = new UpdateFact("(at "+x+" "+y+")", false);
                    UpdateFact e1 = new UpdateFact("(at "+x+" "+z+")", true);
                    ArrayList<UpdateFact> eff = new ArrayList(Arrays.asList(new UpdateFact[]{e0, e1}));
                    
                    UpdateVariable e2 = new UpdateVariable("(energy rover0)", UpdateVariable.OP.DECR, "(traverse_cost "+x+" "+y+" "+z+")");
                    ArrayList<UpdateVariable> effNum = new ArrayList(Arrays.asList(new UpdateVariable[]{e2}));
                    
                    actions.add(new Action(name, pre, eff, effNum));
                }
            }
        }
        // SAMPLE_SOIL
        for (String x : rovers) {
            for (String s : stores) {
                for (String p : waypoints) {
                    String name = "(sample_soil "+x+" "+s+" "+p+")";
                    
                    Condition c0 = new ConditionAssignment("(at "+x+" "+p+")");
                    Condition c1 = new ConditionAssignment("(at_soil_sample "+p+")");
                    Condition c2 = new ConditionAssignment("(equipped_for_soil_analysis "+x+")");
                    Condition c3 = new ConditionAssignment("(store_of "+s+" "+x+")");
                    Condition c4 = new ConditionAssignment("(empty "+s+")");
                    Condition pre = new ConditionAnd(c0, c1, c2, c3, c4);
                    
                    UpdateFact e0 = new UpdateFact("(empty "+s+")", false);
                    UpdateFact e1 = new UpdateFact("(full "+s+")", true);
                    UpdateFact e2 = new UpdateFact("(have_soil_analysis "+x+" "+p+")", true);
                    UpdateFact e3 = new UpdateFact("(at_soil_sample "+p+")", false);
                    ArrayList<UpdateFact> eff = new ArrayList(Arrays.asList(new UpdateFact[]{e0, e1, e2, e3}));
                    
                    actions.add(new Action(name, pre, eff));
                }
            }
        }
        // SAMPLE_ROCK
        for (String x : rovers) {
            for (String s : stores) {
                for (String p : waypoints) {
                    String name = "(sample_rock "+x+" "+s+" "+p+")";
                    
                    Condition c0 = new ConditionAssignment("(at "+x+" "+p+")");
                    Condition c1 = new ConditionAssignment("(at_rock_sample "+p+")");
                    Condition c2 = new ConditionAssignment("(equipped_for_rock_analysis "+x+")");
                    Condition c3 = new ConditionAssignment("(store_of "+s+" "+x+")");
                    Condition c4 = new ConditionAssignment("(empty "+s+")");
                    Condition pre = new ConditionAnd(c0, c1, c2, c3, c4);
                    
                    UpdateFact e0 = new UpdateFact("(empty "+s+")", false);
                    UpdateFact e1 = new UpdateFact("(full "+s+")", true);
                    UpdateFact e2 = new UpdateFact("(have_rock_analysis "+x+" "+p+")", true);
                    UpdateFact e3 = new UpdateFact("(at_rock_sample "+p+")", false);
                    ArrayList<UpdateFact> eff = new ArrayList(Arrays.asList(new UpdateFact[]{e0, e1, e2, e3}));
                    
                    actions.add(new Action(name, pre, eff));
                }
            }
        }
        // DROP
        for (String x : rovers) {
            for (String y : stores) {
                String name = "(drop "+x+" "+y+")";
                
                Condition c0 = new ConditionAssignment("(store_of "+y+" "+x+")");
                Condition c1 = new ConditionAssignment("(full "+y+")");
                Condition pre = new ConditionAnd(c0, c1);

                UpdateFact e0 = new UpdateFact("(full "+y+")", false);
                UpdateFact e1 = new UpdateFact("(empty "+y+")", true);
                ArrayList<UpdateFact> eff = new ArrayList(Arrays.asList(new UpdateFact[]{e0, e1}));

                actions.add(new Action(name, pre, eff));
            }
        }
        // CALIBRATE
        for (String r : rovers) {
            for (String i : cameras) {
                for (String t : objectives) {
                    for (String w : waypoints) {
                        String name = "(calibrate "+r+" "+i+" "+t+" "+w+")";

                        Condition c0 = new ConditionAssignment("(equipped_for_imaging "+r+")");
                        Condition c1 = new ConditionAssignment("(calibration_target "+i+" "+t+")");
                        Condition c2 = new ConditionAssignment("(at "+r+" "+w+")");
                        Condition c3 = new ConditionAssignment("(visible_from "+t+" "+w+")");
                        Condition c4 = new ConditionAssignment("(on_board "+i+" "+r+")");
                        Condition pre = new ConditionAnd(c0, c1, c2, c3, c4);

                        UpdateFact e0 = new UpdateFact("(calibrated "+i+" "+r+")", true);
                        ArrayList<UpdateFact> eff = new ArrayList(Arrays.asList(new UpdateFact[]{e0}));

                        actions.add(new Action(name, pre, eff));
                    }
                }
            }
        }
        // TAKE_IMAGE
        for (String r : rovers) {
            for (String p : waypoints) {
                for (String o : objectives) {
                    for (String i : cameras) {
                        for (String m : modes) {
                            String name = "(take_image "+r+" "+p+" "+o+" "+i+" "+m+")";

                            Condition c0 = new ConditionAssignment("(calibrated "+i+" "+r+")");
                            Condition c1 = new ConditionAssignment("(on_board "+i+" "+r+")");
                            Condition c2 = new ConditionAssignment("(equipped_for_imaging "+r+")");
                            Condition c3 = new ConditionAssignment("(supports "+i+" "+m+")");
                            Condition c4 = new ConditionAssignment("(visible_from "+o+" "+p+")");
                            Condition c5 = new ConditionAssignment("(at "+r+" "+p+")");
                            Condition pre = new ConditionAnd(c0, c1, c2, c3, c4, c5);


                            UpdateFact e0 = new UpdateFact("(have_image "+r+" "+o+" "+m+")", true);
                            UpdateFact e1 = new UpdateFact("(calibrated "+i+" "+r+")", false);
                            ArrayList<UpdateFact> eff = new ArrayList(Arrays.asList(new UpdateFact[]{e0, e1}));

                            actions.add(new Action(name, pre, eff));
                        }
                    }
                }
            }
        }
        // COMMUNICATE_SOIL_DATA
        for (String r : rovers) {
            for (String l : landers) {
                for (String p : waypoints) {
                    for (String x : waypoints) {
                        for (String y : waypoints) {
                            String name = "(communicate_soil_data "+r+" "+l+" "+p+" "+x+" "+y+")";

                            Condition c0 = new ConditionAssignment("(at "+r+" "+x+")");
                            Condition c1 = new ConditionAssignment("(at_lander "+l+" "+y+")");
                            Condition c2 = new ConditionAssignment("(have_soil_analysis "+r+" "+p+")");
                            Condition c3 = new ConditionAssignment("(visible "+x+" "+y+")");
                            Condition c4 = new ConditionAssignment("(available "+r+")");
                            Condition c5 = new ConditionAssignment("(channel_free "+l+")");
                            Condition pre = new ConditionAnd(c0, c1, c2, c3, c4, c5);

                            UpdateFact e0 = new UpdateFact("(communicated_soil_data "+p+")", true);
                            ArrayList<UpdateFact> eff = new ArrayList(Arrays.asList(new UpdateFact[]{e0}));

                            actions.add(new Action(name, pre, eff));
                        }
                    }
                }
            }
        }
        // COMMUNICATE_ROCK_DATA
        for (String r : rovers) {
            for (String l : landers) {
                for (String p : waypoints) {
                    for (String x : waypoints) {
                        for (String y : waypoints) {
                            String name = "(communicate_rock_data "+r+" "+l+" "+p+" "+x+" "+y+")";

                            Condition c0 = new ConditionAssignment("(at "+r+" "+x+")");
                            Condition c1 = new ConditionAssignment("(at_lander "+l+" "+y+")");
                            Condition c2 = new ConditionAssignment("(have_rock_analysis "+r+" "+p+")");
                            Condition c3 = new ConditionAssignment("(visible "+x+" "+y+")");
                            Condition c4 = new ConditionAssignment("(available "+r+")");
                            Condition c5 = new ConditionAssignment("(channel_free "+l+")");
                            Condition pre = new ConditionAnd(c0, c1, c2, c3, c4, c5);

                            UpdateFact e0 = new UpdateFact("(communicated_rock_data "+p+")", true);
                            ArrayList<UpdateFact> eff = new ArrayList(Arrays.asList(new UpdateFact[]{e0}));

                            actions.add(new Action(name, pre, eff));
                        }
                    }
                }
            }
        }
        // COMMUNICATE_IMAGE_DATA
        for (String r : rovers) {
            for (String l : landers) {
                for (String o : objectives) {
                    for (String m : modes) {
                        for (String x : waypoints) {
                            for (String y : waypoints) {
                                String name = "(communicate_image_data "+r+" "+l+" "+o+" "+m+" "+x+" "+y+")";

                                Condition c0 = new ConditionAssignment("(at "+r+" "+x+")");
                                Condition c1 = new ConditionAssignment("(at_lander "+l+" "+y+")");
                                Condition c2 = new ConditionAssignment("(have_image "+r+" "+o+" "+m+")");
                                Condition c3 = new ConditionAssignment("(visible "+x+" "+y+")");
                                Condition c4 = new ConditionAssignment("(available "+r+")");
                                Condition c5 = new ConditionAssignment("(channel_free "+l+")");
                                Condition pre = new ConditionAnd(c0, c1, c2, c3, c4, c5);

                                UpdateFact e0 = new UpdateFact("(communicated_image_data "+o+" "+m+")", true);
                                ArrayList<UpdateFact> eff = new ArrayList(Arrays.asList(new UpdateFact[]{e0}));

                                actions.add(new Action(name, pre, eff));
                            }
                        }
                    }
                }
            }
        }
        
        if (verbose) {
            System.out.println("And " + actions.size() + " actions.\n");
        }
        return actions;
    }
    
    @Override 
    public Condition globalConditions() {
        return new ConditionConstraint("(energy rover0)", ConditionConstraint.OP.GE, 0); //(always (>= (energy rover0) 0))
    }
    
    private int closingParenthesisIndex(int startIndex, String str) {
        int count = 0;
        for (int i = startIndex; i < str.length(); i++) {
            if (str.charAt(i) == '(') {
                count += 1;
            } else if (str.charAt(i) == ')') {
                count -= 1;
                if (count == 0) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    private List<String> split(String str) {
        List<String> args = new ArrayList<>();
        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) == '(') {
                int j = closingParenthesisIndex(i,str);
                args.add(str.substring(i,j+1));
                i = j;
            }
        }
        return args;
    }
    
    private String readFile(String filename) {
        String content = "";
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String line;
            while ((line = reader.readLine()) != null) {
                content += " " + line;
            }
            reader.close();
            content = cleanString(content);
            
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", filename);
            e.printStackTrace();
            content = null;
        }
        return content;
    }
    
    private String cleanString(String from) {
        String to = from.replaceAll("\n", " ")
                        .replaceAll("\t", " ");
        int beforeLength = -1;
        while (beforeLength != to.length()) {
            beforeLength = to.length();
            to = to.replaceAll("  ", " ");
        }
        to = to.replaceAll("\\( ", "(")
               .replaceAll(" \\)", ")");
        return to;
    }
    
    private void init(String filename) {
        String file = readFile(filename);
        
        int i0, g0, m0, o0, i1, g1, m1, o1;
        i0 = file.indexOf("(:init ");
        g0 = file.indexOf("(:goal ");
        m0 = file.indexOf("(:metric ");
        o0 = file.indexOf("(:objects ");
        i1 = closingParenthesisIndex(i0, file);
        g1 = closingParenthesisIndex(g0, file);
        m1 = closingParenthesisIndex(m0, file);
        o1 = closingParenthesisIndex(o0, file);
        
        init    = file.substring(i0,i1+1);
        goal    = file.substring(g0,g1+1);
        metric  = file.substring(m0,m1+1);
        objects = file.substring(o0,o1+1);
    }
}
