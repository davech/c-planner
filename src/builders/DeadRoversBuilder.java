
package builders;

import model.*;
import java.util.*;

/**
 *
 * @author zombi
 */
public class DeadRoversBuilder extends RoversBuilder {
    public DeadRoversBuilder(String filename) {
        super(filename);
    }
    public DeadRoversBuilder(String filename, boolean v) {
        super(filename, v);
    }
    
    @Override
    public State initialState() {
        State initialState = super.initialState();
        return initialState.setVariable(new NumericVariable("(energy rover0)", 0));
    }
    
    @Override
    public ArrayList<Action> actions() {
        ArrayList<Action> allActions = super.actions();
        ArrayList<Action> actions = new ArrayList<>();
        for (Action a : allActions) {
            if (!a.getName().startsWith("(navigate ")) {
                actions.add(a);
            }
        }
        return actions;
    }
}
