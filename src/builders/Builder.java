
package builders; //MetricSimplePreference

import model.*;
import colesplanner.*;
import java.util.*;

/**
 *
 * @author Charline David 
 */
public abstract class Builder {
    public static double THETA = 0.99;
    public static double THETA2 = 0.5;
    
    protected boolean verbose;
    
    public abstract State initialState();
    public abstract ArrayList<Goal> goals();
    public abstract ArrayList<Action> actions();
    public abstract Condition globalConditions();
    
    public PlanningProblem planningProblem() {
        PlanningProblem prob = new PlanningProblem( initialState(), 
                                                    goals(), 
                                                    actions(),
                                                    globalConditions(),
                                                    THETA );
        prob.setVerbose(verbose);
        prob.ready();
        return prob;
    }
}