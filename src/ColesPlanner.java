import builders.*;
import colesplanner.*;
import graphs.TreeNode;
import java.util.*;

/**
 * @author Charline David 
 */
public class ColesPlanner {
    private static final String defaultPrefix = "C:\\Users\\zombi\\Documents\\uqam\\0 INF6200 Initiation � la recherche\\my_prog\\pddl\\MetricSimplePreferences\\";
    private static final List<String> defaultProblems = Arrays.asList(new String[]{"p01.pddl","p02.pddl","p03.pddl","p04.pddl","p05.pddl","p06.pddl","p07.pddl","p08.pddl","p09.pddl","p10.pddl"});
    
    public static void main(String[] args) {
        int runs = 1;
        boolean outSamples = false;
        boolean outPlans = false;
        boolean verbose = false;
        String prefix = "";
        List<String> problems = new LinkedList<>();
        
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                if (args[i].equals("-runs")) {
                    runs = Integer.parseInt(args[++i]);
                } else if (args[i].equals("-prefix")) {
                    prefix = args[++i];
                    if (!prefix.endsWith("\\") && !prefix.endsWith("/")) {
                        prefix += "\\";
                    }
                } else if (args[i].equals("-outSamples")) {
                    outSamples = true;
                } else if (args[i].equals("-outPlans")) {
                    outPlans = true;
                } else if (args[i].equals("-out")) {
                    outSamples = true;
                    outPlans = true;
                } else if (args[i].equals("-v") || args[i].equals("-verbose")) {
                    verbose = true;
                }  else if (args[i].equals("-default:prefix")) {
                    prefix = defaultPrefix;
                }
                
            } else {
                problems.add(args[i]);
            }
        }
        
        List<Sample> samples;
        if (problems.isEmpty()) {
            samples = run(defaultPrefix, defaultProblems, runs, outPlans, verbose);
        } else {
            samples = run(prefix, problems, runs, outPlans, verbose);
        }
        
        if (outSamples && !samples.isEmpty()) {
            try {
                downloadSamples(samples);
            } catch (IOException e) {}
        }
    }
    
    public static List<Sample> run(String prefix, List<String> problems, int RUNS, boolean outPlans, boolean verbose) {        
        List<Sample> samples = new ArrayList<>();
        
        for (String suffix : problems) {
            Builder builder = new RoversBuilder(prefix + suffix, verbose);
            PlanningProblem prob = builder.planningProblem();
            Sample sample = new Sample(suffix, prob.goalsCost());
            
            while (sample.nbRuns() < RUNS) {
                prob.reset();
                System.gc();
                
                TreeNode<Policy> plan = prob.branchPlan();
                prob.simulate(plan, sample);
                
                if (outPlans && plan != null) {
                    System.out.println(plan.toStringAlignLeft());
                }
                System.out.println(sample.plainData());
            }
            
            samples.add(sample);
            prob.close();
            System.gc();
            
            System.out.println("\n" + Sample.titles());
            for (Sample s : samples) 
                System.out.println(s.report());
            System.out.println();
        }
        
        return samples;
    }
    
    public static void downloadSamples(List<Sample> samples) throws IOException {
        for (Sample s : samples) {
            String name = s.problemName();
            if (name.contains(".")) {
                name = name.substring(0, name.lastIndexOf("."));
            }
            if (name.contains("\\")) {
                name = name.substring(name.lastIndexOf("\\")+1, name.length());
            }
            if (name.contains("/")) {
                name = name.substring(name.lastIndexOf("/")+1, name.length());
            }
            FileWriter fileWriter = new FileWriter("C:\\Users\\zombi\\Desktop\\" + name + "_data.csv");
            fileWriter.write(s.csv());
            fileWriter.close();
        }
    }
}
